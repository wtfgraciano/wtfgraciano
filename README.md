# Graciano Codes

Hi, my name is Graciano (or you can call me Jimi) and I write code since 2008. I mainly use gitlab for personal projects, studying and currently I (very rarely) stream what I'm coding (mostly in portuguese) at [twitch](https://www.twitch.tv/camaradajimi).

## Projects

The most relevant project I'm working on right now is [a parliament vote count for the Brazillian proportional vote system](https://gitlab.com/wtfgraciano/eleicoes-resultados-proporcionais). I've started this at the 2022 election and I expect it to fully work on the 2024 municipal elections. Other relevant repositories are:

 - [Ludista de Tarefas](https://gitlab.com/wtfgraciano/tired-todo): an anti-productivity todo list, hosted on https://web.ludistadetarefas.net
 - [embaralha](https://gitlab.com/wtfgraciano/embaralha): an HTML suffle effect that I use on [my personal site](https://graciano.me)

I have a lot of old abandoned projects in this profile. If for whatever reason you find any of them interesting, I'll be happy to revive them, or just accept a MR, or even transfer them to you. You can just @ me on mastodon, send me an e-mail or just make a MR.

